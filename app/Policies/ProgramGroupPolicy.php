<?php

namespace App\Policies;

use App\User;
use App\ProgramGroup;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProgramGroupPolicy
{
    use HandlesAuthorization;
    
    public function create(User $user)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function update(User $user, ProgramGroup $programGroup)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function delete(User $user, ProgramGroup $programGroup)
    {
        return ($user->isChief() || $user->isAdmin());
    }
}
