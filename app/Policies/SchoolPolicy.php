<?php

namespace App\Policies;

use App\User;
use App\School;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchoolPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function update(User $user, School $school)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function delete(User $user, School $school)
    {
        return ($user->isChief() || $user->isAdmin());
    }
}
