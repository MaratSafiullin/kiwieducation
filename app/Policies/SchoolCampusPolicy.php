<?php

namespace App\Policies;

use App\User;
use App\SchoolCampus;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchoolCampusPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function update(User $user, SchoolCampus $schoolCampus)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function delete(User $user, SchoolCampus $schoolCampus)
    {
        return ($user->isChief() || $user->isAdmin());
    }
}
