<?php

namespace App\Policies;

use App\User;
use App\Branch;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user, Branch $branch)
    {
        return $user->isAdmin();
    }

    public function delete(User $user, Branch $branch)
    {
        return $user->isAdmin();
    }
}
