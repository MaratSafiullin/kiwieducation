<?php

namespace App\Policies;

use App\User;
use App\SchoolContactPerson;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchoolContactPersonPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function update(User $user, SchoolContactPerson $schoolContactPerson)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function delete(User $user, SchoolContactPerson $schoolContactPerson)
    {
        return ($user->isChief() || $user->isAdmin());
    }
}
