<?php

namespace App\Policies;

use App\User;
use App\Program;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProgramPolicy
{
    use HandlesAuthorization;
    
    public function create(User $user)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function update(User $user, Program $program)
    {
        return ($user->isChief() || $user->isAdmin());
    }

    public function delete(User $user, Program $program)
    {
        return ($user->isChief() || $user->isAdmin());
    }
}
