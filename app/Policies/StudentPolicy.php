<?php

namespace App\Policies;

use App\User;
use App\Student;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, Student $student)
    {
        return ($user->isChief() || $user->isAdmin());
    }
}
