<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class School extends Model
{
    protected $fillable = [
        'name',
        'type_id'
    ];

    public function type()
    {
        return $this->belongsTo('App\SchoolType');
    }

    public function campuses()
    {
        return $this->hasMany('App\SchoolCampus');
    }

    public function contactPersons()
    {
        return $this->hasMany('App\SchoolContactPerson');
    }

    public function commissions()
    {
        return $this->hasMany('App\SchoolCommission');
    }

    public function programs()
    {
        return $this->hasMany('App\Program');
    }

    public static function filterSchools($schoolName = null, $locationName = null, $schoolTypeId = null)
    {
        $query = DB::table('schools');

        if ($schoolTypeId) {
            $query->where('type_id', "{$schoolTypeId}");
        }

        if ($schoolName) {
            $query->where('name', 'like', "%{$schoolName}%");
        }

        //school has campus, its city or country name is like $locationName
        if ($locationName) {
            $campusIds = SchoolCampus::filterCampuses($locationName)->pluck('id');

            $query->join('school_campuses', function ($join) use ($campusIds) {
                $join->on('school_campuses.school_id', '=', 'schools.id')
                    ->whereIn('school_campuses.id', $campusIds);
            });
        }

        $ids = $query->pluck('schools.id')->all();

        return (School::find($ids));
    }
}
