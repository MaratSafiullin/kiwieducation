<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Program extends Model
{
    protected $fillable = [
        'name',
        'link',
        'level_id',
        'school_id',
        'group_id',
        'commission_rate'
    ];

    public function level()
    {
        return $this->belongsTo('App\ProgramLevel');
    }

    public function school()
    {
        return $this->belongsTo('App\School');
    }

    public function group()
    {
        return $this->belongsTo('App\ProgramGroup');
    }

    public function campuses()
    {
        return $this->belongsToMany('App\SchoolCampus');
    }

    public static function filterPrograms($programName = null, $locationName = null, $programGroupId = null,
                                          $levelIds = null, $schoolName = null)
    {
        $query = DB::table('programs');

        if ($programGroupId) {
            $query->where('group_id', "{$programGroupId}");
        }

        if ($programName) {
            $query->where('name', 'like', "%{$programName}%");
        }

        if ($levelIds) {
            $query->whereIn('level_id', $levelIds);
        }

        //program has campus, its city or country name is like $locationName
        if ($locationName) {
            $campusIds = SchoolCampus::filterCampuses($locationName)->pluck('id')->all();

            $query->join('program_school_campus', function ($join) use ($campusIds) {
                $join->on('program_school_campus.program_id', '=', 'programs.id')
                    ->whereIn('program_school_campus.school_campus_id', $campusIds);
            });
        }

        //program belongs to school with name like $schoolName
        if ($schoolName) {
            $schoolIds = School::filterSchools($schoolName)->pluck('id')->all();

            $query->join('schools', function ($join) use ($schoolIds) {
                $join->on('schools.id', '=', 'programs.school_id')
                    ->whereIn('schools.id', $schoolIds);
            });
        }

        $ids = $query->pluck('programs.id')->all();

        return (Program::find($ids));
    }
}
