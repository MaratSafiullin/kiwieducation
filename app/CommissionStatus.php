<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommissionStatus extends Model
{
    protected $fillable = [
        'name'
    ];
}
