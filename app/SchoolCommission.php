<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolCommission extends Model
{
    protected $fillable = [
        'name',
        'rate'
    ];

    public function school() {
        return $this->belongsTo('App\School');
    }
}
