<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $fillable = [
        'manager_id',
        'branch_id',
        'school_id',
        'program_id',
        'study_status_id',
        'duration',
        'duration_unit_id',
        'start_date',
        'offer_issued_date',
        'cost',
        'agency_commission',
        'agency_commission_collection_date',
        'agency_commission_status_id',
        'chief_commission',
        'chief_commission_status_id',
        'chief_commission_collection_date',
        'manager_commission',
        'manager_commission_status_id',
        'manager_commission_collection_date'
    ];

    protected $dates = ['visa_due_date', 'insurance_due_date'];

    public function setPublishedAtAttribute($date) {
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
    }
    
    public function student() {
        return $this->belongsTo('App\Student');
    }

    public function manager() {
        return $this->belongsTo('App\User' , 'manager_id');
    }

    public function branch() {
        return $this->belongsTo('App\Branch');
    }

    public function school() {
        return $this->belongsTo('App\School');
    }

    public function program() {
        return $this->belongsTo('App\Program');
    }

    public function status() {
        return $this->belongsTo('App\StudyStatus');
    }

    public function durationUnit() {
        return $this->belongsTo('App\DurationUnit');
    }

    public function agencyCommissionStatus() {
        return $this->belongsTo('App\CommissionStatuses', 'agency_commission_status_id');
    }

    public function chiefCommissionStatus() {
        return $this->belongsTo('App\CommissionStatuses', 'chief_commission_status_id');
    }

    public function managerCommissionStatus() {
        return $this->belongsTo('App\CommissionStatuses', 'manager_commission_status_id');
    }
}
