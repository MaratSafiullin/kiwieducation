<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Student::class => \App\Policies\StudentPolicy::class,
        \App\School::class => \App\Policies\SchoolPolicy::class,
        \App\SchoolCampus::class => \App\Policies\SchoolCampusPolicy::class,
        \App\SchoolContactPerson::class => \App\Policies\SchoolContactPersonPolicy::class,
        \App\Program::class => \App\Policies\ProgramPolicy::class,
        \App\ProgramGroup::class => \App\Policies\ProgramGroupPolicy::class,
        \App\Branch::class => \App\Policies\BranchPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
