<?php

namespace App\Providers;

use App\Branch;
use App\City;
use App\CommissionStatus;
use App\Country;
use App\DurationUnit;
use App\InsuranceType;
use App\Program;
use App\ProgramGroup;
use App\ProgramLevel;
use App\School;
use App\SchoolCampus;
use App\SchoolContactPerson;
use App\SchoolType;
use App\Student;
use App\StudentSource;
use App\StudentStatus;
use App\Study;
use App\StudyStatus;
use App\User;
use App\VisaType;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();

        Route::model('city', City::class);
        Route::model('commission_status', CommissionStatus::class);
        Route::model('country', Country::class);
        Route::model('duration_unit', DurationUnit::class);
        Route::model('insurance_type', InsuranceType::class);
        Route::model('program_group', ProgramGroup::class);
        Route::model('program_level', ProgramLevel::class);
        Route::model('school_type', SchoolType::class);
        Route::model('student_source', StudentSource::class);
        Route::model('student_status', StudentStatus::class);
        Route::model('study_status', StudyStatus::class);
        Route::model('visa_types', VisaType::class);

        Route::model('branch', Branch::class);
        Route::model('manager', User::class);
        Route::model('program', Program::class);
        Route::model('school', School::class);
        Route::model('campus', SchoolCampus::class);
        Route::model('contact_person', SchoolContactPerson::class);
        Route::model('student', Student::class);
        Route::model('study', Study::class);
        Route::model('user', User::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}
