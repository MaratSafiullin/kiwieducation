<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DurationUnit extends Model
{
    protected $fillable = [
        'name'
    ];
}
