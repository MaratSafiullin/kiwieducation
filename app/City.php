<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'name'
    ];

    public function schools() {
        return $this->belongsToMany('App\School');
    }

    public static function filterCities($name)
    {
        return City::where('name', 'like', "%{$name}%")->get();
    }
}
