<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'name'
    ];

    public static function filterCities($name)
    {
        return Country::where('name', 'like', "%{$name}%")->get();
    }
}
