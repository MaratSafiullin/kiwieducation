<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_role_id',
        'name',
        'last_name',
        'email',
        'password',
        'country_id',
        'city_id',
        'skype',
        'phone',
        'social_networks',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles() {
        return $this->hasMany('App\Article');
    }

    public function role() {
        return $this->belongsTo('App\UserRole', 'user_role_id');
    }

    public function branches() {
        return $this->belongsToMany('App\Branch', 'branch_manager', 'manager_id', 'branch_id');
    }

    public function isAdmin() {
        return $this->role && $this->role->name === 'Admin';
    }

    public function isChief() {
        return $this->role && $this->role->name === 'Chief';
    }

    public static function filterUsers($name)
    {
        return User::where('name', 'like', "%{$name}%")->orWhere('last_name', 'like', "%{$name}%")->get();
    }
}
