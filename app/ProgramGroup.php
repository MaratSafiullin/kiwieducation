<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProgramGroup extends Model
{
    protected $fillable = [
        'name'
    ];

    public function programs() {
        return $this->hasMany('App\Program');
    }

    public static function filterProgramGroups($groupName = null, $schoolName = null)
    {
        $query = DB::table('program_groups');

        if ($groupName) {
            $query->where('name', 'like', "%{$groupName}%");
        }

        //program group has program which belongs to school with name like $locationName
        if ($schoolName) {
            $schoolIds = School::filterSchools($schoolName)->pluck('id');

            $query->join('programs', function ($join) use ($schoolIds) {
                $join->on('programs.group_id', '=', 'groups.id')
                    ->whereIn('programs.school_id', $schoolIds);
            });
        }

        $ids = $query->pluck('program_groups.id')->all();

        return (ProgramGroup::find($ids));
    }
}
