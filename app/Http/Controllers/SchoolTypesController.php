<?php

namespace App\Http\Controllers;

use App\SchoolType;
use Request;

use App\Http\Requests;

class SchoolTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $schoolTypes = SchoolType::get();

        return view('school-types.index', compact('schoolTypes'));
    }

    public function store()
    {
        $schoolType = new SchoolType(Request::all());

        $schoolType->save();

        return redirect('school-types');
    }

    public function update($schoolType)
    {
        $schoolType->update(Request::all());

        return redirect('school-types');
    }

    public function destroy($schoolType)
    {
        $schoolType->delete();

        return redirect('school-types');
    }
}
