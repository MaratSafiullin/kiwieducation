<?php

namespace App\Http\Controllers;

use App\StudentStatus;
use Request;

use App\Http\Requests;

class StudentStatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $statuses = StudentStatus::get();

        return view('student-statuses.index', compact('statuses'));
    }

    public function store()
    {
        $status = new StudentStatus(Request::all());

        $status->save();

        return redirect('student-statuses');
    }

    public function update($status)
    {
        $status->update(Request::all());

        return redirect('student-statuses');
    }

    public function destroy($status)
    {
        $status->delete();

        return redirect('student-statuses');
    }
}
