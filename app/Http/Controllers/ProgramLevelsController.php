<?php

namespace App\Http\Controllers;

use App\ProgramLevel;
use Request;

use App\Http\Requests;

class ProgramLevelsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $levels = ProgramLevel::get();

        return view('program-levels.index', compact('levels'));
    }

    public function store()
    {
        $level = new ProgramLevel(Request::all());

        $level->save();

        return redirect('program-levels');
    }

    public function update($level)
    {
        $level->update(Request::all());

        return redirect('program-levels');
    }

    public function destroy($level)
    {
        $level->delete();

        return redirect('program-levels');
    }
}
