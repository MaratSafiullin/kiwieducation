<?php

namespace App\Http\Controllers;

use App\UserRole;
use Request;

use App\Http\Requests;

class UserRolesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
//        $this->middleware('admin');
    }

    public function index()
    {
        $userRoles = UserRole::get();

        return view('user-roles.index', compact('userRoles'));
    }

    public function store()
    {
        $userRole = new UserRole(Request::all());

        $userRole->save();

        return redirect('user-roles');
    }

    public function update($id)
    {
        $userRole = UserRole::findOrFail($id);

        $userRole->update(Request::all());

        return redirect('user-roles');
    }

    public function destroy($id)
    {
        $userRole = UserRole::findOrFail($id);

        $userRole->delete();

        return redirect('user-roles');
    }
}
