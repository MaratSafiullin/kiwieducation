<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\SchoolRequest;
use App\Program;
use App\School;
use App\SchoolCommission;
use App\SchoolType;
use Illuminate\Support\Facades\DB;
use Request;

class SchoolsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $typesArr = Controller::searchArray(SchoolType::pluck('name', 'id'));

        $schoolName = Request::input('name');
        $locationName = Request::input('location');
        $schoolTypeId = Request::input('type_id');

        $schools = School::filterSchools($schoolName, $locationName, $schoolTypeId);

        return view('schools.index', compact('schools', 'typesArr', 'schoolName', 'locationName', 'schoolTypeId'));
    }

    public function show($school)
    {
        $typesArr = SchoolType::pluck('name', 'id');

        return view('schools.show', compact('school', 'typesArr'));
    }

    public function edit($school)
    {
        $this->authorize('update', $school);

        $typesArr = SchoolType::pluck('name', 'id');

        return view('schools.edit', compact('school', 'typesArr'));
    }

    public function store(SchoolRequest $request)
    {
        $this->authorize('create', School::class);

        $school = new School($request->all());

        $school->save();

        return redirect('schools');
    }

    public function update($school, SchoolRequest $request)
    {
        $this->authorize('update', $school);

        $school->update($request->all());

        return redirect("schools/{$school->id}/edit");
    }

    public function destroy($school)
    {
        $this->authorize('delete', $school);

        $school->delete();

        return redirect('schools');
    }
}
