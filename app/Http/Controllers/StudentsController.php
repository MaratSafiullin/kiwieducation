<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Http\Requests\StudentRequest;
use App\InsuranceType;
use App\Program;
use App\ProgramGroup;
use App\School;
use App\Student;
use App\StudentSource;
use App\StudentStatus;
use App\VisaType;
use Request;

use App\Http\Requests;

class StudentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $firstName = Request::input('first_name');
        $lastName = Request::input('last_name');
        $locationName = Request::input('location');
        $contactText = Request::input('contact');
        $dateAddedFrom = Request::input('date_added_from');
        $dateAddedTo = Request::input('date_added_to');
        $studyLocationName = Request::input('study_location');
        $schoolName = Request::input('school_name');
        $programGroupId = Request::input('program_group_id');
        $sourceName = Request::input('source');
        $onlyMy = (Request::input('only_my') === 'yes');
        $statusIds = Request::input('status_ids');

        $groupsArr = Controller::searchArray(ProgramGroup::pluck('name', 'id'));
        $studentStatusesArr = StudentStatus::pluck('name', 'id');

        $students = Student::filterStudents($firstName, $lastName, $locationName, $contactText, $dateAddedFrom,
            $dateAddedTo, $studyLocationName, $schoolName, $programGroupId, $sourceName, $onlyMy, $statusIds);

        return view('students.index', compact('students', 'groupsArr', 'studentStatusesArr', 'firstName', 'lastName', 'locationName', 'contactText',
            'dateAddedFrom', 'dateAddedTo', 'studyLocationName', 'schoolName', 'programGroupId', 'sourceName',
            'onlyMy', 'statusIds'));
    }

    public function edit($student)
    {
        $sourcesArr = Controller::selectWithEmptyValArray(StudentSource::pluck('name', 'id'));
        $statusesArr = StudentStatus::pluck('name', 'id');
        $visaTypesArr = VisaType::pluck('name', 'id');
        $insuranceTypesArr = InsuranceType::pluck('name', 'id');
        $countriesArr = Country::pluck('name', 'id');
        $citiesArr = City::pluck('name', 'id');

        $schoolsArr = School::pluck('name', 'id');
        $programsArr = Program::pluck('name', 'id');

        return view('students.edit', compact('student', 'sourcesArr', 'statusesArr',
            'visaTypesArr', 'insuranceTypesArr', 'countriesArr', 'citiesArr', 'schoolsArr',
            'programsArr'));
    }

    public function store(StudentRequest $request)
    {
        $student = new Student($request->all());

        $student->save();

        return redirect("students/{$student->id}/edit");
    }

    public function update($student, StudentRequest $request)
    {
        $student->update($request->all());

        return redirect("students/{$student->id}/edit");
    }

    public function destroy($student)
    {
        $this->authorize('delete', $student);

        $student->delete();

        return redirect("students");
    }
}
