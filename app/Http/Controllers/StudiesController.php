<?php

namespace App\Http\Controllers;

use App\Branch;
use App\CommissionStatus;
use App\DurationUnit;
use App\Program;
use App\School;
use App\Student;
use App\StudentStatus;
use App\Study;
use App\StudyStatus;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Auth;
use Request;

use App\Http\Requests;

class StudiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($student, $study)
    {
        $schoolsArr = School::pluck('name', 'id');
        $programsArr = Program::pluck('name', 'id');
        $branchesArr = Branch::pluck('name', 'id');
        $managersArr = $this->getManagersArr($study);
        $studyStatusesArr = StudentStatus::pluck('name', 'id');
        $durationUnitsArr = DurationUnit::pluck('name', 'id');
        $commissionStatusesArr = CommissionStatus::pluck('name', 'id');

        return view('studies.edit', compact('student', 'study', 'schoolsArr', 'programsArr',
            'branchesArr', 'managersArr', 'studyStatusesArr', 'durationUnitsArr', 'commissionStatusesArr'));
    }

    public function store($student)
    {
        $manager = Auth::user();
        $branch = Auth::user()->branches()->first();

        $study = new Study(Request::all());
        $study->manager()->associate($manager);
        $study->branch()->associate($branch);
        $student->studies()->save($study);

        return redirect("students/{$student->id}/edit");
    }

    public function update($student, $study)
    {
        $study->update(Request::all());

        return redirect("students/{$student->id}/studies/{$study->id}/edit");
    }

    public function destroy($student, $study)
    {
        $study->delete();

        return redirect("students/{$student->id}/edit");
    }

    private function getManagersArr($study)
    {
        $studyManagerId = $study->manager_id;
        $managerRoleId = UserRole::where('name', 'Manager')->first()->id;
        $managersArr = User::where('user_role_id', $managerRoleId)->
        orWhere('id', $studyManagerId)->pluck('name', 'id');

        return $managersArr;
    }


}
