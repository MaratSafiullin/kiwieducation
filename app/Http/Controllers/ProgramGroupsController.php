<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProgramGroupRequest;
use App\ProgramGroup;
use Request;

use App\Http\Requests;

class ProgramGroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $groupName = Request::input('name');
        $schoolName = Request::input('school_name');

        $programGroups = ProgramGroup::filterProgramGroups($groupName, $schoolName);

        return view('program-groups.index', compact('programGroups', 'groupName', 'schoolName'));
    }

    public function store(ProgramGroupRequest $request)
    {
        $programGroup = new ProgramGroup($request->all());
        
        $programGroup->save();

        return redirect('program-groups');
    }

    public function update($programGroup, ProgramGroupRequest $request)
    {
        $programGroup->update($request->all());

        return redirect('program-groups');
    }

    public function destroy($programGroup)
    {
        $programGroup->delete();

        return redirect('program-groups');
    }
}
