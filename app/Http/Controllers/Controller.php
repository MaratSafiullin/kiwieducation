<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected static function searchArray(Collection $collection)
    {
        return ([0 => '...Any...'] + $collection->all());
    }

    protected static function selectWithEmptyValArray(Collection $collection)
    {
        return ([0 => ''] + $collection->all());
    }
}
