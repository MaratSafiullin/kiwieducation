<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\User;
use App\UserRole;
use Request;

use App\Http\Requests;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('admin');
    }

    public function index()
    {
        $users = User::get();

        return view('users.index', compact('users'));
    }

    public function edit($user)
    {
        $rolesArr = UserRole::pluck('name', 'id');
        $countriesArr = Country::pluck('name', 'id');
        $citiesArr = City::pluck('name', 'id');

        return view('users.edit', compact('user', 'rolesArr', 'countriesArr', 'citiesArr'));
    }

    public function update($user)
    {
        $user->update(Request::all());

        return redirect("users/{$user->id}/edit");
    }
}
