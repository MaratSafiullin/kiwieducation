<?php

namespace App\Http\Controllers;

use App\VisaType;
use Request;

use App\Http\Requests;

class VisaTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $types = VisaType::get();

        return view('visa-types.index', compact('types'));
    }

    public function store()
    {
        $type = new VisaType(Request::all());

        $type->save();

        return redirect('visa-types');
    }

    public function update($type)
    {
        $type->update(Request::all());

        return redirect('visa-types');
    }

    public function destroy($type)
    {
        $type->delete();

        return redirect('visa-types');
    }
}
