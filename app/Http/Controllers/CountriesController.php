<?php

namespace App\Http\Controllers;

use App\Country;
use Request;

use App\Http\Requests;

class CountriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $countries = Country::get();

        return view('countries.index', compact('countries'));
    }

    public function store()
    {
        $country = new Country(Request::all());

        $country->save();

        return redirect('countries');
    }

    public function update($country)
    {
        $country->update(Request::all());

        return redirect('countries');
    }

    public function destroy($country)
    {
        $country->delete();

        return redirect('countries');
    }
}
