<?php

namespace App\Http\Controllers;

use App\DurationUnit;
use Request;

use App\Http\Requests;

class DurationUnitsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $durationUnits = DurationUnit::get();

        return view('duration-units.index', compact('durationUnits'));
    }

    public function store()
    {
        $durationUnit = new DurationUnit(Request::all());

        $durationUnit->save();

        return redirect('duration-units');
    }

    public function update($durationUnit)
    {
        $durationUnit->update(Request::all());

        return redirect('duration-units');
    }

    public function destroy($durationUnit)
    {
        $durationUnit->delete();

        return redirect('duration-units');
    }
}
