<?php

namespace App\Http\Controllers;

use App\StudentSource;
use Request;

use App\Http\Requests;

class StudentSourcesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $sources = StudentSource::get();

        return view('student-sources.index', compact('sources'));
    }

    public function store()
    {
        $source = new StudentSource(Request::all());

        $source->save();

        return redirect('student-sources');
    }

    public function update($source)
    {
        $source->update(Request::all());

        return redirect('student-sources');
    }

    public function destroy($source)
    {
        $source->delete();

        return redirect('student-sources');
    }
}
