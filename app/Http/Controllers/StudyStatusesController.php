<?php

namespace App\Http\Controllers;

use App\StudyStatus;
use Request;

use App\Http\Requests;

class StudyStatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $studyStatuses = StudyStatus::get();

        return view('study-statuses.index', compact('studyStatuses'));
    }

    public function store()
    {
        $studyStatus = new StudyStatus(Request::all());

        $studyStatus->save();

        return redirect('study-statuses');
    }

    public function update($studyStatus)
    {
        $studyStatus->update(Request::all());

        return redirect('study-statuses');
    }

    public function destroy($studyStatus)
    {
        $studyStatus->delete();

        return redirect('study-statuses');
    }
}
