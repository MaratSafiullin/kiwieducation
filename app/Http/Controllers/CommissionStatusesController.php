<?php

namespace App\Http\Controllers;

use App\CommissionStatus;
use App\Http\Requests\CommissionStatusRequest;
use Request;

use App\Http\Requests;

class CommissionStatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $commissionStatuses = CommissionStatus::get();

        return view('commission-statuses.index', compact('commissionStatuses'));
    }

    public function store(CommissionStatusRequest $request)
    {
        $commissionStatus = new CommissionStatus($request->all());
        
        $commissionStatus->save();

        return redirect('commission-statuses');
    }

    public function update($commissionStatus, CommissionStatusRequest $request)
    {
        $commissionStatus->update($request->all());

        return redirect('commission-statuses');
    }

    public function destroy($commissionStatus)
    {
        $commissionStatus->delete();

        return redirect('commission-statuses');
    }
}
