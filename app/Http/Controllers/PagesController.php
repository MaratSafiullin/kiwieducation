<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function about() {

        $names = ['Me', 'notMe', 'someone'];
        return view('pages/about', compact('names'));
    }
}
