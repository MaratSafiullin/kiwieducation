<?php

namespace App\Http\Controllers;

use App\Branch;
use App\City;
use App\Country;
use App\Http\Requests\BranchRequest;
use App\User;
use App\UserRole;
use Request;

use App\Http\Requests;

class BranchesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('chief');
    }

    public function index()
    {
        $chiefsArr = $this->getChiefsArr();
        $chiefsArrSearch = Controller::searchArray($this->getChiefsArr());
        $managersArr = Controller::searchArray($this->getManagersArr());

        $chiefId = Request::input('chief_id');
        $managerId = Request::input('manager_id');
        $userName = Request::input('user_name');

        $branches = Branch::filterBranches($chiefId, $managerId, $userName);
        
        return view('branches.index', compact('branches', 'chiefsArr', 'chiefsArrSearch', 'managersArr', 'chiefId', 'managerId', 'userName'));
    }

    public function show($branch)
    {
        $chiefsArr = $this->getChiefsArr();
        $managersArr = $this->getManagersArr();
        $countriesArr = Country::pluck('name', 'id');
        $citiesArr = City::pluck('name', 'id');

        return view('branches.show', compact('branch', 'managersArr', 'chiefsArr', 'countriesArr', 'citiesArr'));
    }

    public function edit($branch)
    {
        $this->authorize('update', $branch);

        $chiefsArr = $this->getChiefsArr();
        $managersArr = $this->getManagersArr();
        $countriesArr = Country::pluck('name', 'id');
        $citiesArr = City::pluck('name', 'id');
        
        return view('branches.edit', compact('branch', 'managersArr', 'chiefsArr', 'countriesArr', 'citiesArr'));
    }

    /**
     * @param Requests\BranchRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BranchRequest $request)
    {
        $this->authorize('create', Branch::class);

        $branch = new Branch($request->all());
        $branch->save();

        return redirect('branches');
    }

    public function update($branch, BranchRequest $request)
    {
        $this->authorize('update', $branch);

        $branch->update($request->all());

        return redirect("branches/{$branch->id}/edit");
    }

    public function destroy($branch)
    {
        $this->authorize('delete', $branch);

        $branch->delete();

        return redirect('branches');
    }

    public function addManager($branch)
    {
        $this->authorize('update', $branch);

        $managerId = Request::input('manager_id');

        $branch->managers()->attach($managerId);

        return redirect("branches/{$branch->id}/edit");
    }

    public function removeManager($branch, $manager)
    {
        $this->authorize('update', $branch);

        $branch->managers()->detach($manager->id);

        return redirect("branches/{$branch->id}/edit");
    }

    private function getChiefsArr()
    {
        $chiefRoleId = UserRole::where('name', 'Chief')->first()->id;
        $chiefsArr = User::where('user_role_id', $chiefRoleId)->pluck('name', 'id');

        return $chiefsArr;
    }

    private function getManagersArr()
    {
        $managerRoleId = UserRole::where('name', 'Manager')->first()->id;
        $managersArr = User::where('user_role_id', $managerRoleId)->pluck('name', 'id');

        return $managersArr;
    }
}
