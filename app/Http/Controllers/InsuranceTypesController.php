<?php

namespace App\Http\Controllers;

use App\InsuranceType;
use Request;

use App\Http\Requests;

class InsuranceTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $types = InsuranceType::get();

        return view('insurance-types.index', compact('types'));
    }

    public function store()
    {
        $type = new InsuranceType(Request::all());

        $type->save();

        return redirect('insurance-types');
    }

    public function update($type)
    {
        $type->update(Request::all());

        return redirect('insurance-types');
    }

    public function destroy($type)
    {
        $type->delete();

        return redirect('insurance-types');
    }
}
