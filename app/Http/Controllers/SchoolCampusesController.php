<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\School;
use App\SchoolCampus;
use Illuminate\Support\Facades\Auth;
use Request;

use App\Http\Requests;

class SchoolCampusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($school, $campus)
    {
        $countriesArr = Country::pluck('name', 'id');
        $citiesArr = City::pluck('name', 'id');

        return view('school-campuses.show', compact('school', 'campus', 'countriesArr', 'citiesArr'));
    }

    public function edit($school, $campus)
    {
        $this->authorize('update', $campus);

        $countriesArr = Country::pluck('name', 'id');
        $citiesArr = City::pluck('name', 'id');

        return view('school-campuses.edit', compact('school', 'campus', 'countriesArr', 'citiesArr'));
    }
    
    public function store($school)
    {
        $this->authorize('create', SchoolCampus::class);
        
        $campus = new SchoolCampus(Request::all());

        $school->campuses()->save($campus);

        return redirect("schools/{$school->id}/edit");
    }

    public function update($school, $campus)
    {
        $this->authorize('update', $campus);

        $campus->update(Request::all());

        return redirect("schools/{$school->id}/campuses/{$campus->id}/edit");
    }

    public function destroy($school, $campus)
    {
        $this->authorize('delete', $campus);
        
        $campus->delete();

        return redirect("schools/{$school->id}/edit");
    }
}
