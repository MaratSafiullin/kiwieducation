<?php

namespace App\Http\Controllers;

use App\School;
use App\SchoolCampus;
use App\SchoolContactPerson;
use Request;

use App\Http\Requests;

class SchoolContactPersonsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($school, $contactPerson)
    {
        $campusesArr = SchoolCampus::pluck('name', 'id');

        return view('school-contact-persons.show', compact('school', 'contactPerson', 'campusesArr'));
    }

    public function edit($school, $contactPerson)
    {
        $this->authorize('update', $contactPerson);

        $campusesArr = SchoolCampus::pluck('name', 'id');

        return view('school-contact-persons.edit', compact('school', 'contactPerson', 'campusesArr'));
    }

    public function store($school)
    {
        $this->authorize('create', SchoolContactPerson::class);

        $contactPerson = new SchoolContactPerson(Request::all());

        $school->contactPersons()->save($contactPerson);

        return redirect("schools/{$school->id}/edit");
    }

    public function update($school, $contactPerson)
    {
        $this->authorize('update', $contactPerson);

        $contactPerson->update(Request::all());

        return redirect("schools/{$school->id}/contact-persons/{$contactPerson->id}/edit");
    }

    public function destroy($school, $contactPerson)
    {
        $this->authorize('delete', $contactPerson);

        $contactPerson->delete();

        return redirect("schools/{$school->id}/edit");
    }
}
