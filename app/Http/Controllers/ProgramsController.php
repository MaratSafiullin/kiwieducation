<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProgramRequest;
use App\Program;
use App\ProgramGroup;
use App\ProgramLevel;
use App\School;
use App\SchoolCampus;
use Request;

use App\Http\Requests;

class ProgramsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $programName = Request::input('name');
        $locationName = Request::input('location');
        $programGroupId = Request::input('group_id');
        $levelIds = Request::input('level_ids');

        $groupsArr = Controller::searchArray(ProgramGroup::pluck('name', 'id'));
        $levelsArr = ProgramLevel::pluck('name', 'id');

        $programs = Program::filterPrograms($programName, $locationName, $programGroupId, $levelIds);

        return view('programs.index', compact('programs', 'groupsArr', 'levelsArr', 'programName', 'locationName',
            'programGroupId', 'levelIds'));
    }

    public function show($program)
    {
        $levelsArr = ProgramLevel::pluck('name', 'id');
        $groupsArr = ProgramGroup::pluck('name', 'id');
        $schoolsArr = School::pluck('name', 'id');
        $campusesArr = SchoolCampus::pluck('name', 'id');

        return view('programs.show', compact('program', 'levelsArr', 'groupsArr', 'schoolsArr', 'campusesArr'));
    }

    public function edit($program)
    {
        $this->authorize('update', $program);

        $levelsArr = ProgramLevel::pluck('name', 'id');
        $groupsArr = ProgramGroup::pluck('name', 'id');
        $schoolsArr = School::pluck('name', 'id');
        $campusesArr = SchoolCampus::pluck('name', 'id');

        return view('programs.edit', compact('program', 'levelsArr', 'groupsArr', 'schoolsArr', 'campusesArr'));
    }

    public function store(ProgramRequest $request)
    {
        $this->authorize('create', Program::class);

        $program = new Program($request->all());
        $program->save();

        return redirect('programs');
    }

    public function update($program, ProgramRequest $request)
    {
        $this->authorize('update', $program);

        $program->update($request->all());

        return redirect("programs/{$program->id}/edit");
    }

    public function destroy($program)
    {
        $this->authorize('delete', $program);

        $program->delete();

        return redirect('programs');
    }

    public function addCampus($program)
    {
        $this->authorize('update', $program);

        $campusId = Request::input('campus_id');

        $program->campuses()->attach($campusId);

        return redirect("programs/{$program->id}/edit");
    }

    public function removeCampus($program, $campus)
    {
        $this->authorize('update', $program);

        $program->campuses()->detach($campus->id);

        return redirect("programs/{$program->id}/edit");
    }
}
