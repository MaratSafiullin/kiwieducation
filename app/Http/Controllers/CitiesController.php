<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\CityRequest;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Requests;

class CitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $cities = City::get();

        return view('cities.index', compact('cities'));
    }

    public function store(CityRequest $request)
    {
        $city = new City($request->all());
        $city->save();

        return redirect('cities');
    }

    public function update($city, CityRequest $request)
    {
        $city->update($request->all());

        return redirect('cities');
    }

    public function destroy($city)
    {
        $city->delete();

        return redirect('cities');
    }
}
