<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'country_id',
        'city_id',
        'email',
        'skype',
        'phone',
        'social_networks',
        'source',
        'status_id',
        'visa_type_id',
        'visa_due_date',
        'visa_comment',
        'insurance_type_id',
        'insurance_due_date',
        'insurance_comment'
    ];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function studies()
    {
        return $this->hasMany('App\Study');
    }

    public static function filterStudents($firstName = null, $lastName = null, $locationName = null, $contactText = null,
                                          $dateAddedFrom = null, $dateAddedTo = null, $studyLocationName = null,
                                          $schoolName = null, $programGroupId = null, $sourceName = null, $onlyMy = null,
                                          $statusIds = null)
    {
        $query = DB::table('students');

        if ($firstName) {
            $query->where('first_name', 'like', "%{$firstName}%");
        }

        if ($lastName) {
            $query->where('last_name', 'like', "%{$lastName}%");
        }

        if ($dateAddedFrom) {
            $query->where('created_at', '>=', $dateAddedFrom);
        }

        if ($dateAddedTo) {
            //set time to 23:59:59
            $dateAddedTo .= ' 23:59:59'; //!!consider better way

            $query->where('created_at', '<=', $dateAddedTo);
        }

        if ($statusIds) {
            $query->whereIn('status_id', $statusIds);
        }

        if ($sourceName) {
            $query->where('source', 'like', "%{$sourceName}%");
        }

        //student with city or country name like $locationName
        if ($locationName) {
            $cityIds = City::filterCities($locationName)->pluck('id')->all();
            $countryIds = Country::filterCities($locationName)->pluck('id')->all();

            $query->where(function ($query) use ($cityIds, $countryIds) {
                $query->where(function ($query) use ($cityIds) {
                    $query->whereIn('city_id', $cityIds);
                })->orWhere(function ($query) use ($countryIds) {
                    $query->whereIn('country_id', $countryIds);
                });
            });
        }

        //student has study with program in location like $studyLocationName and in school named like $schoolName and from group $programGroupId
        if ($studyLocationName || $schoolName || $programGroupId) {
            $programIds = Program::filterPrograms(null, $studyLocationName, $programGroupId, null, $schoolName)->pluck('id')->all();

            $query->join('studies', function ($join) use ($programIds) {
                $join->on('studies.student_id', '=', 'students.id')
                    ->whereIn('studies.program_id', $programIds);
            });
        }

        $ids = $query->pluck('students.id')->all();

        return (Student::find($ids));
    }
}
