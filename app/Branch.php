<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Branch extends Model
{
    protected $fillable = [
        'name',
        'country_id',
        'city_id',
        'chief_id'
    ];

    public function chief()
    {
        return $this->belongsTo('App\User', 'chief_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function managers()
    {
        return $this->belongsToMany('App\User', 'branch_manager', 'branch_id', 'manager_id');
    }

    public static function filterBranches($chiefId = null, $managerId = null, $userName = null)
    {
        $query = DB::table('branches');

        if ($chiefId) {
            $query->where('chief_id', $chiefId);
        }

        if ($managerId) {
            $query->join('branch_manager', function ($join) use ($managerId) {
                $join->on('branch_manager.branch_id', '=', 'branches.id')
                    ->where('branch_manager.manager_id', $managerId);
            });
        }

        //branch has chief or manager who's name or last name is like $userName
        if ($userName) {
            $userIds = User::filterUsers($userName)->pluck('id');

            if (!$managerId) {
                $query->join('branch_manager', 'branch_manager.branch_id', '=', 'branches.id');
            }

            $query->where(function ($query) use ($userIds) {
                $query->whereIn('branches.chief_id', $userIds);
            })->orWhere(function ($query) use ($userIds) {
                $query->whereIn('branch_manager.manager_id', $userIds);
            });
        }

        $ids = $query->pluck('branches.id')->all();

        return (Branch::find($ids));
    }
}
