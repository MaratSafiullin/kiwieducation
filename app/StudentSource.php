<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSource extends Model
{
    protected $fillable = [
        'name'
    ];
}
