<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolContactPerson extends Model
{
    protected $fillable = [
        'name',
        'role',
        'email',
        'skype',
        'phone',
        'social_networks',
        'comment',
        'campus_id'
    ];

    public function school() {
        return $this->belongsTo('App\School');
    }

    public function campus() {
        return $this->belongsTo('App\SchoolCampus');
    }
}
