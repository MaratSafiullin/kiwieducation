<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolCampus extends Model
{
    protected $fillable = [
        'name',
        'comment',
        'country_id',
        'city_id'
    ];

    public function school()
    {
        return $this->belongsTo('App\School');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public static function filterCampuses($locationName = null)
    {
        return SchoolCampus::where(function ($query) use ($locationName) {

            //campus with city or country name like $locationName
            if ($locationName) {
                $cityIds = City::filterCities($locationName)->pluck('id')->all();
                $countryIds = Country::filterCities($locationName)->pluck('id')->all();

                $query->where(function ($query) use ($cityIds) {
                    $query->whereIn('city_id', $cityIds);
                })->orWhere(function ($query) use ($countryIds) {
                    $query->whereIn('country_id', $countryIds);
                });
            }

        })->get();
    }
}
