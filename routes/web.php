<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('about', 'PagesController@about');

Auth::routes();

Route::get('/home', 'HomeController@index');

//Route::get('articles', 'ArticlesController@index');
//Route::get('articles/create', 'ArticlesController@create');
//Route::get('articles/{article}', 'ArticlesController@show');
//Route::post('articles', 'ArticlesController@store');
//Route::get('articles/{article}/edit', 'ArticlesController@edit');

Route::resource('articles', 'ArticlesController');

Route::resource('user-roles', 'UserRolesController');

Route::resource('users', 'UsersController');

Route::resource('branches', 'BranchesController');

Route::post('branches/{branch}/managers', 'BranchesController@addManager')
    ->name('branches.add-manager');
Route::delete('branches/{branch}/managers/{manager}', 'BranchesController@removeManager')
    ->name('branches.remove-manager');

Route::resource('cities', 'CitiesController');

Route::resource('countries', 'CountriesController');

Route::resource('program-groups', 'ProgramGroupsController');

Route::resource('school-types', 'SchoolTypesController');

Route::resource('student-statuses', 'StudentStatusesController');

Route::resource('student-sources', 'StudentSourcesController');

Route::resource('visa-types', 'VisaTypesController');

Route::resource('insurance-types', 'InsuranceTypesController');

Route::resource('program-levels', 'ProgramLevelsController');

Route::resource('programs', 'ProgramsController');

Route::post('programs/{program}/campuses', 'ProgramsController@addCampus')
    ->name('programs.add-campus');
Route::delete('programs/{program}/campuses/{campus}', 'ProgramsController@removeCampus')
    ->name('programs.remove-campus');

Route::resource('students', 'StudentsController');

Route::post('students/{student}/studies', 'StudiesController@store')
    ->name('students.studies.store');
Route::get('students/{student}/studies/{study}/edit', 'StudiesController@edit')
    ->name('students.studies.edit');
Route::put('students/{student}/studies/{study}', 'StudiesController@update')
    ->name('students.studies.update');
Route::delete('schools/{student}/studies/{study}', 'StudiesController@destroy')
    ->name('students.studies.destroy');

Route::resource('schools', 'SchoolsController');

//Route::post('schools/{school}/cities', 'SchoolsController@addCity')
//    ->name('schools.add-city');
//Route::delete('schools/{school}/cities/{city}', 'SchoolsController@removeCity')
//    ->name('schools.remove-city');

//Route::post('schools/{school}/commissions', 'SchoolCommissionsController@store')
//    ->name('schools.commissions.store');
//Route::put('schools/{school}/commissions/{commission}', 'SchoolCommissionsController@update')
//    ->name('schools.commissions.update');
//Route::delete('schools/{school}/commissions/{commission}', 'SchoolCommissionsController@destroy')
//    ->name('schools.commissions.destroy');

Route::post('schools/{school}/campuses', 'SchoolCampusesController@store')
    ->name('schools.campuses.store');
Route::get('schools/{school}/campuses/{campus}', 'SchoolCampusesController@show')
    ->name('schools.campuses.show');
Route::get('schools/{school}/campuses/{campus}/edit', 'SchoolCampusesController@edit')
    ->name('schools.campuses.edit');
Route::put('schools/{school}/campuses/{campus}', 'SchoolCampusesController@update')
    ->name('schools.campuses.update');
Route::delete('schools/{school}/campuses/{campus}', 'SchoolCampusesController@destroy')
    ->name('schools.campuses.destroy');

Route::post('schools/{school}/contact-persons', 'SchoolContactPersonsController@store')
    ->name('schools.contact-persons.store');
Route::get('schools/{school}/contact-persons/{contact_person}', 'SchoolContactPersonsController@show')
    ->name('schools.contact-persons.show');
Route::get('schools/{school}/contact-persons/{contact_person}/edit', 'SchoolContactPersonsController@edit')
    ->name('schools.contact-persons.edit');
Route::put('schools/{school}/contact-persons/{contact_person}', 'SchoolContactPersonsController@update')
    ->name('schools.contact-persons.update');
Route::delete('schools/{school}/contact-persons/{contact_person}', 'SchoolContactPersonsController@destroy')
    ->name('schools.contact-persons.destroy');

Route::post('schools/{school}/programs', 'ProgramsController@store')
    ->name('schools.programs.store');
Route::put('schools/{school}/programs/{program}', 'ProgramsController@update')
    ->name('schools.programs.update');
Route::delete('schools/{school}/programs/{program}', 'ProgramsController@destroy')
    ->name('schools.programs.destroy');

Route::resource('study-statuses', 'StudyStatusesController');

Route::resource('commission-statuses', 'CommissionStatusesController');

Route::resource('duration-units', 'DurationUnitsController');

//Route::resource('customer-statuses', 'CustomerStatusesController');
//
//Route::resource('customers', 'CustomersController');

//Route::post('customers/{customer}/comments', 'CustomersController@createComment')
//    ->name('customers.create-comment');

//Route::get('customers/{customer}/studies/create', 'CustomerStudiesController@create')
//    ->name('customers.studies.create');
//Route::post('customers/{customer}/studies', 'CustomerStudiesController@store')
//    ->name('customers.studies.store');
//Route::get('customers/{customer}/studies/{study}/edit', 'CustomerStudiesController@edit')
//    ->name('customers.studies.edit');
//Route::put('customers/{customer}/studies/{study}', 'CustomerStudiesController@update')
//    ->name('customers.studies.update');
//Route::delete('customers/{customer}/studies/{study}', 'CustomerStudiesController@destroy')
//    ->name('customers.studies.destroy');