<div class="row">
    <div class="col-xs-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Country name']) !!}
    </div>
    <div class="col-xs-6">
        {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>
