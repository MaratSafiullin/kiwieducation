@extends('layouts.app')

@section('content')
    <h1>Insurance Types</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'insurance-types']) !!}
        @include('insurance-types._form', ['submitButtonText' => 'Add Type'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($types as $type)
                <tr>
                    <td>
                        {!! Form::model($type, ['method' => 'PUT', 'route' => ['insurance-types.update', $type->id]]) !!}
                        @include('insurance-types._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['insurance-types.destroy', $type->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop