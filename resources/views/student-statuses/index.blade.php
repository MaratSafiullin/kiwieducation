@extends('layouts.app')

@section('content')
    <h1>Student Statuses</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'student-statuses']) !!}
        @include('student-statuses._form', ['submitButtonText' => 'Add Status'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($statuses as $status)
                <tr>
                    <td>
                        {!! Form::model($status, ['method' => 'PUT', 'route' => ['student-statuses.update', $status->id]]) !!}
                        @include('student-statuses._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['student-statuses.destroy', $status->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop