@extends('layouts.app')

@section('content')
    <h1>{{$school->name}} {{$contactPerson->name}}</h1>

    <hr/>

    <div class="form-group">
        <a href="/schools/{{$school->id}}/edit" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($contactPerson, ['method' => 'PUT', 'route' => ['schools.contact-persons.update', $school->id, $contactPerson->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('name', 'Person name:') !!}
                {!! Form::text('name', null, ['disabled' => 'disabled', 'class' => 'form-control',
                'placeholder'=>'Person name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('role', 'Role:') !!}
                {!! Form::text('role', null, ['disabled' => 'disabled', 'class' => 'form-control',
                                'placeholder'=>'Role']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('campus_id', 'Campus:') !!}
                {!! Form::select('campus_id', $campusesArr, null, ['disabled' => 'disabled', 'class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-3">
                        {!! Form::label('email', 'Email:') !!}
                    </div>
                    <div class="col-xs-9">
                        {!! Form::email('email', null, ['disabled' => 'disabled', 'class' => 'form-control',
                        'placeholder'=>'Email']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        {!! Form::label('phone', 'Phone:') !!}
                    </div>
                    <div class="col-xs-9">
                        {!! Form::text('phone', null, ['disabled' => 'disabled', 'class' => 'form-control',
                        'placeholder'=>'Phone']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        {!! Form::label('skype', 'Skype:') !!}
                    </div>
                    <div class="col-xs-9">
                        {!! Form::text('skype', null, ['disabled' => 'disabled', 'class' => 'form-control',
                        'placeholder'=>'Skype']) !!}
                    </div>
                </div>
            </div>

            <div class="col-xs-6">
                {!! Form::label('social_networks', 'Social networks links:') !!}
                {!! Form::textarea('social_networks', null, ['disabled' => 'disabled', 'size' => '30x3', 'class' => 'form-control',
                'placeholder'=>'Social networks links']) !!}
            </div>
        </div>


        <div class="form-group row">
            {!! Form::textarea('comment', null, ['disabled' => 'disabled', 'size' => '30x4', 'class' => 'form-control', 'placeholder'=>'Comment']) !!}
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@stop