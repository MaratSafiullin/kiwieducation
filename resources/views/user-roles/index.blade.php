@extends('layouts.app')

@section('content')
    <h1>User Roles</h1>
    <hr/>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'user-roles']) !!}
        @include('user-roles._form', ['submitButtonText' => 'Add Role'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($userRoles as $role)
                <tr>
                    <td>
                        {!! Form::model($role, ['method' => 'PUT', 'route' => ['user-roles.update', $role->id]]) !!}
                        @include('cities._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['user-roles.destroy', $role->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop