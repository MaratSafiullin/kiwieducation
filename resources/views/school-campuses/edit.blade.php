@extends('layouts.app')

@section('content')
    <h1>{{$school->name}} {{$campus->name}}</h1>

    <hr/>

    <div class="form-group">
        <a href="/schools/{{$school->id}}/edit" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($campus, ['method' => 'PUT', 'route' => ['schools.campuses.update', $school->id, $campus->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('name', 'Campus name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control',
                'placeholder'=>'Campus name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('country_id', 'Country:') !!}
                {!! Form::select('country_id', $countriesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('city_id', 'City:') !!}
                {!! Form::select('city_id', $citiesArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            {!! Form::textarea('comment', null, ['size' => '30x4', 'class' => 'form-control', 'placeholder'=>'Comment']) !!}
        </div>

        <div class="row">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@stop