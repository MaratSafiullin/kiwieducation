@extends('layouts.app')

@section('content')
    <h1>{{$student->first_name}} {{$student->last_name}}</h1>

    <hr/>

    <div class="row">
        <div class="col-xs-2">
            <div class="form-group">
                <a href="/students" class="btn btn-success">Back</a>
            </div>
        </div>
        <div class="col-xs-2 col-xs-offset-8">
            {!! Form::open(['method' => 'DELETE', 'route' => ['students.destroy', $student->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($student, ['method' => 'PUT', 'route' => ['students.update', $student->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('status_id', 'Status:') !!}
                {!! Form::select('status_id', $statusesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('source', 'Source:') !!}
                {!! Form::text('source', null, ['class' => 'form-control',
                                'placeholder' => 'Source', 'id' => 'student-source-textfield']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label(null, '...or choose from list:') !!}
                {!! Form::select(null, $sourcesArr, null, ['class' => 'form-control', 'id' => 'student-source-select']) !!}
            </div>
        </div>

        <hr/>

        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('first_name', 'First name:') !!}
                {!! Form::text('first_name', null, ['class' => 'form-control',
                'placeholder'=>'First name']) !!}

                {!! Form::label('last_name', 'Last name:') !!}
                {!! Form::text('last_name', null, ['class' => 'form-control',
                'placeholder'=>'Last name']) !!}

                {!! Form::label('country_id', 'Country:') !!}
                {!! Form::select('country_id', $countriesArr, null, ['class' => 'form-control']) !!}

                {!! Form::label('city_id', 'City:') !!}
                {!! Form::select('city_id', $citiesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('visa_type_id', 'Visa:') !!}
                {!! Form::select('visa_type_id', $visaTypesArr, null, ['class' => 'form-control']) !!}

                {!! Form::label('visa_due_date', 'Visa due date:') !!}
                {!! Form::input('date', 'visa_due_date', null,
                ['class' => 'form-control', 'placeholder'=>'Visa due date']) !!}

                {!! Form::label('visa_comment', 'Comment:') !!}
                {!! Form::textarea('visa_comment', null,
                ['size' => '30x4', 'class' => 'form-control', 'placeholder'=>'Comment']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('insurance_type_id', 'Insurance:') !!}
                {!! Form::select('insurance_type_id', $insuranceTypesArr, null, ['class' => 'form-control']) !!}

                {!! Form::label('insurance_due_date', 'Insurance due date:') !!}
                {!! Form::input('date', 'insurance_due_date', null,
                ['class' => 'form-control', 'placeholder'=>'Insurance due date']) !!}

                {!! Form::label('insurance_comment', 'Comment:') !!}
                {!! Form::textarea('insurance_comment', null,
                ['size' => '30x4', 'class' => 'form-control', 'placeholder'=>'Comment']) !!}
            </div>
        </div>

        <hr/>

        <div class="form-group row">
            <div class="col-xs-3">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder'=>'Email']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('phone', 'Phone:') !!}
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder'=>'Phone']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('skype', 'Skype:') !!}
                {!! Form::text('skype', null, ['class' => 'form-control', 'placeholder'=>'Skype']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::textarea('social_networks', null, ['size' => '30x2', 'class' => 'form-control',
                'placeholder'=>'Social networks links']) !!}
            </div>
        </div>

        <div class="row">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <h3>{{$student->name}} Studies</h3>

        {!! Form::open(['method' => 'POST', 'route' => ['students.studies.store', $student->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::select('school_id', $schoolsArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::select('program_id', $programsArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::submit('Add Study', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($student->studies as $study)
                <tr>
                    <td>
                        {{$study->school->name}}
                    </td>
                    <td>
                        {{$study->program->name}}
                    </td>
                    <td>
                        <a href="/students/{{$student->id}}/studies/{{$study->id}}/edit" class="btn btn-success form-control">Edit</a>
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['students.studies.destroy', $student->id, $study->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop