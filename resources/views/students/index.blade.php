@extends('layouts.app')

@section('content')
    <div class=" text-center">
        <div class="row form-group">
            {!! Form::open(['method' => 'GET', 'route' => 'students.index']) !!}
            <div class="col-xs-2">
                {!! Form::label('first_name', 'First name:') !!}
                {!! Form::text('first_name', $firstName, ['class' => 'form-control', 'placeholder'=>'First name']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('last_name', 'Last name:') !!}
                {!! Form::text('last_name', $lastName, ['class' => 'form-control', 'placeholder'=>'Last name']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('location', 'Country/City:') !!}
                {!! Form::text('location', $locationName, ['class' => 'form-control', 'placeholder'=>'Location']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('contact', 'Contact(partial):') !!}
                {!! Form::text('contact', $contactText, ['disabled', 'class' => 'form-control', 'placeholder'=>'Contact(partial)']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('date_added_from', 'Added after:') !!}
                {!! Form::input('date', 'date_added_from', $dateAddedFrom, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('date_added_to', 'Added before:') !!}
                {!! Form::input('date', 'date_added_to', $dateAddedTo, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-3">
                {!! Form::label('study_location', 'Study Country/City:') !!}
                {!! Form::text('study_location', $studyLocationName, ['class' => 'form-control', 'placeholder'=>'Study Location']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('school_name', 'School name:') !!}
                {!! Form::text('school_name', $schoolName, ['class' => 'form-control', 'placeholder'=>'School name']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('program_group_id', 'Program Group:') !!}
                {!! Form::select('program_group_id', $groupsArr, $programGroupId, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('source', 'Source:') !!}
                {!! Form::text('source', $sourceName, ['class' => 'form-control', 'placeholder'=>'Source']) !!}
            </div>
            <div class="col-xs-1">
                {!! Form::label('only_my', 'Only my:') !!}
                {!! Form::checkbox('only_my', 'yes', $onlyMy, ['disabled']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">
                {!! Form::label('status_ids', 'Satuses:') !!}
            </div>
            <div class="col-xs-6">
                {!! Form::select('status_ids[]', $studentStatusesArr, $statusIds, ['id' => 'status-ids', 'class' => 'form-control', 'multiple']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::submit('Search', ['class' => 'btn btn-default form-control']) !!}
            </div>
            {!! Form::close() !!}
            <div class="col-xs-2">
                <a href="/students" class="btn btn-default  form-control">Reset</a>
            </div>
        </div>
    </div>

    <h1>Students</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'students']) !!}
        <div class="row">
            <div class="col-xs-4">
                {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder'=>'First name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder'=>'Last name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::submit('Add Student', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($students as $student)
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-xs-4">
                                {{$student->first_name}}
                            </div>
                            <div class="col-xs-4">
                                {{$student->last_name}}
                            </div>
                            <div class="col-xs-4">
                                <a href="/students/{{$student->id}}/edit"
                                   class="btn btn-success form-control">Edit</a>
                            </div>
                        </div>
                    </td>
                    {{--<td>--}}
                    {{--{!! Form::open(['method' => 'DELETE', 'route' => ['students.destroy', $student->id]]) !!}--}}
                    {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}--}}
                    {{--{!! Form::close() !!}--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop