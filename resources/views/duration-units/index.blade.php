@extends('layouts.app')

@section('content')
    <h1>Duration Units</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'duration-units']) !!}
        @include('duration-units._form', ['submitButtonText' => 'Add Unit'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($durationUnits as $unit)
                <tr>
                    <td>
                        {!! Form::model($unit, ['method' => 'PUT', 'route' => ['duration-units.update', $unit->id]]) !!}
                        @include('duration-units._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['duration-units.destroy', $unit->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop