@extends('layouts.app')

@section('content')
    <h1>{{$branch->name}}</h1>

    <hr/>

    <div class="row">
        <div class="col-xs-2">
            <div class="form-group">
                <a href="/branches" class="btn btn-success">Back</a>
            </div>
        </div>
        <div class="col-xs-2 col-xs-offset-8">
            {!! Form::open(['method' => 'DELETE', 'route' => ['branches.destroy', $branch->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($branch, ['method' => 'PUT', 'route' => ['branches.update', $branch->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-3">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Branch name']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('chief_id', 'Chief:') !!}
                {!! Form::select('chief_id', $chiefsArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('countries_id', 'Country:') !!}
                {!! Form::select('countries_id', $countriesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('city_id', 'City:') !!}
                {!! Form::select('city_id', $citiesArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="row">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <h3>{{$branch->name}} Managers</h3>

        {!! Form::open(['method' => 'POST', 'route' => ['branches.add-manager', $branch->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-6">
                {!! Form::select('manager_id', $managersArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-6">
                {!! Form::submit('Add Manager', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($branch->managers as $manager)
                <tr>
                    <td>
                        {{$manager->name}}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['branches.remove-manager', $branch->id, $manager->id]]) !!}
                        {!! Form::submit('Remove', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop