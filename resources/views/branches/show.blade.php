@extends('layouts.app')

@section('content')
    <h1>{{$branch->name}}</h1>

    <hr/>

    <div class="form-group">
        <a href="/branches" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($branch, ['method' => 'PUT', 'route' => ['branches.update', $branch->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-3">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['disabled', 'class' => 'form-control', 'placeholder'=>'Branch name']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('chief_id', 'Chief:') !!}
                {!! Form::select('chief_id', $chiefsArr, null, ['disabled', 'class' => 'form-control']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('countries_id', 'Country:') !!}
                {!! Form::select('countries_id', $countriesArr, null, ['disabled', 'class' => 'form-control']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('city_id', 'City:') !!}
                {!! Form::select('city_id', $citiesArr, null, ['disabled', 'class' => 'form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <h3>{{$branch->name}} Managers</h3>

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($branch->managers as $manager)
                <tr>
                    <td>
                        {{$manager->name}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop