@extends('layouts.app')

@section('content')
    <div class="row">
        {!! Form::open(['method' => 'GET', 'route' => 'branches.index']) !!}
        <div class="col-xs-3">
            {!! Form::label('chief_id', 'Chief:') !!}
            {!! Form::select('chief_id', $chiefsArrSearch, $chiefId, ['class' => 'form-control']) !!}
        </div>
        <div class="col-xs-3">
            {!! Form::label('manager_id', 'Manager:') !!}
            {!! Form::select('manager_id', $managersArr, $managerId, ['class' => 'form-control']) !!}
        </div>
        <div class="col-xs-3">
            {!! Form::label('user_name', 'Chief/Manager name:') !!}
            {!! Form::text('user_name', $userName, ['class' => 'form-control', 'placeholder'=>'Chief/Manager name']) !!}
        </div>
        <div class="col-xs-2">
            {!! Form::label('', '&nbsp;') !!}
            {!! Form::submit('Search', ['class' => 'btn btn-default form-control']) !!}
        </div>
        {!! Form::close() !!}
        <div class="col-xs-1">
            {!! Form::label('', '&nbsp;') !!}
            <a href="/branches" class="btn btn-default  form-control">Reset</a>
        </div>
    </div>

    <h1>Branches</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    @can('create', \App\Branch::class)
        <div class="well well-lg text-center">
            {!! Form::open(['url' => 'branches']) !!}
            <div class="row">
                <div class="col-xs-4">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Branch name']) !!}
                </div>
                <div class="col-xs-4">
                    {!! Form::label('chief_id', 'Chief:') !!}
                    {!! Form::select('chief_id', $chiefsArr, null, ['class' => 'form-control']) !!}
                </div>
                <div class="col-xs-4">
                    {!! Form::label('', '&nbsp;') !!}
                    {!! Form::submit('Add Branch', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    @endcan

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($branches as $branch)
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-xs-4">
                                {{$branch->name}}
                            </div>
                            <div class="col-xs-4">
                                {{$branch->chief->name}}
                            </div>
                            <div class="col-xs-4">
                                @can('update', $branch)
                                    <a href="/branches/{{$branch->id}}/edit"
                                       class="btn btn-success form-control">Edit</a>
                                @else
                                    <a href="/branches/{{$branch->id}}"
                                       class="btn btn-default form-control">View</a>
                                @endcan
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop