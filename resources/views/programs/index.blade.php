@extends('layouts.app')

@section('content')
    <div class="row">
        {!! Form::open(['method' => 'GET', 'route' => 'programs.index']) !!}
        <div class="col-xs-2">
            {!! Form::label('name', 'Name(partial):') !!}
            {!! Form::text('name', $programName, ['class' => 'form-control', 'placeholder'=>'Name(partial)']) !!}
        </div>
        <div class="col-xs-2">
            {!! Form::label('location', 'Country/City:') !!}
            {!! Form::text('location', $locationName, ['class' => 'form-control', 'placeholder'=>'Location']) !!}
        </div>
        <div class="col-xs-2">
            {!! Form::label('group_id', 'Group:') !!}
            {!! Form::select('group_id', $groupsArr, $programGroupId, ['class' => 'form-control']) !!}
        </div>
        <div class="col-xs-3">
            {!! Form::label('level_ids', 'Levels:') !!}
            {!! Form::select('level_ids[]', $levelsArr, $levelIds, ['id' => 'level-ids', 'class' => 'form-control', 'multiple']) !!}
        </div>
        <div class="col-xs-2">
            {!! Form::label('', '&nbsp;') !!}
            {!! Form::submit('Search', ['class' => 'btn btn-default form-control']) !!}
        </div>
        {!! Form::close() !!}
        <div class="col-xs-1">
            {!! Form::label('', '&nbsp;') !!}
            <a href="/programs" class="btn btn-default  form-control">Reset</a>
        </div>
    </div>

    <h1>Programs</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    @can('create', \App\Program::class)
        <div class="well well-lg text-center">
            {!! Form::open(['url' => 'programs']) !!}
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Program name']) !!}
                </div>
                <div class="col-xs-6">
                    {!! Form::submit('Add Program', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    @endcan

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($programs as $program)
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-xs-6">
                                {{$program->name}}
                            </div>
                            <div class="col-xs-6">
                                @can('update', $program)
                                    <a href="/programs/{{$program->id}}/edit"
                                       class="btn btn-success form-control">Edit</a>
                                @else
                                    <a href="/programs/{{$program->id}}"
                                       class="btn btn-default form-control">View</a>
                                @endcan
                            </div>

                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop