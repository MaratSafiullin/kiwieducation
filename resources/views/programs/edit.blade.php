@extends('layouts.app')

@section('content')
    <h1>{{$program->name}}</h1>

    <hr/>

    <div class="row">
        <div class="col-xs-2">
            <div class="form-group">
                <a href="/programs" class="btn btn-success">Back</a>
            </div>
        </div>
        <div class="col-xs-2 col-xs-offset-8">
            {!! Form::open(['method' => 'DELETE', 'route' => ['programs.destroy', $program->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($program, ['method' => 'PUT', 'route' => ['programs.update', $program->id]]) !!}
        <div class="row form-group">
            <div class="col-xs-4">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Program name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('link', 'Url:') !!}
                {!! Form::text('link', null, ['class' => 'form-control', 'placeholder'=>'Url']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('commission_rate', 'Commission:') !!}
                {!! Form::text('commission_rate', null, ['class' => 'form-control', 'placeholder'=>'Commission']) !!}
            </div>
        </div>

        <div class="row form-group">
            <div class="col-xs-4">
                {!! Form::label('level_id', 'Level:') !!}
                {!! Form::select('level_id', $levelsArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('group_id', 'Group:') !!}
                {!! Form::select('group_id', $groupsArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('school_id', 'Schools:') !!}
                {!! Form::select('school_id', $schoolsArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="row form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <h3>{{$program->name}} Campuses</h3>

        {!! Form::open(['method' => 'POST', 'route' => ['programs.add-campus', $program->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-6">
                {!! Form::select('campus_id', $campusesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-6">
                {!! Form::submit('Add Campus', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($program->campuses as $campus)
                <tr>
                    <td>
                        {{$campus->name}}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['programs.remove-campus', $program->id, $campus->id]]) !!}
                        {!! Form::submit('Remove', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop