@extends('layouts.app')

@section('content')
    <h1>{{$program->name}}</h1>

    <hr/>

    <div class="form-group">
        <a href="/programs" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($program, ['method' => 'PUT', 'route' => ['programs.update', $program->id]]) !!}
        <div class="row form-group">
            <div class="col-xs-4">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['disabled', 'class' => 'form-control', 'placeholder'=>'Program name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('link', 'Url:') !!}
                {!! Form::text('link', null, ['disabled', 'class' => 'form-control', 'placeholder'=>'Url']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('commission_rate', 'Commission:') !!}
                {!! Form::text('commission_rate', null, ['disabled', 'class' => 'form-control', 'placeholder'=>'Commission']) !!}
            </div>
        </div>

        <div class="row form-group">
            <div class="col-xs-4">
                {!! Form::label('level_id', 'Level:') !!}
                {!! Form::select('level_id', $levelsArr, null, ['disabled', 'class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('group_id', 'Group:') !!}
                {!! Form::select('group_id', $groupsArr, null, ['disabled', 'class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('school_id', 'Schools:') !!}
                {!! Form::select('school_id', $schoolsArr, null, ['disabled', 'class' => 'form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <h3>{{$program->name}} Campuses</h3>

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($program->campuses as $campus)
                <tr>
                    <td>
                        {{$campus->name}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop