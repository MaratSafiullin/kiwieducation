@extends('layouts.app')

@section('content')
    <h1>Users</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-xs-4">
                                {{$user->email}}
                            </div>
                            <div class="col-xs-4">
                                {{$user->name}}
                            </div>
                            <div class="col-xs-4">
                                <a href="/users/{{$user->id}}/edit" class="btn btn-success form-control">Edit</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop