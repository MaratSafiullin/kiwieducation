@extends('layouts.app')

@section('content')
    <h1>{{$user->name}}</h1>

    <hr/>

    <div class="form-group">
        <a href="/users" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update', $user->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder'=>'Email']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder'=>'Last name']) !!}
            </div>
        </div>

        <hr/>

        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('user_role_id', 'User role:') !!}
                {!! Form::select('user_role_id', $rolesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('country_id', 'Country:') !!}
                {!! Form::select('country_id', $countriesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('city_id', 'City:') !!}
                {!! Form::select('city_id', $citiesArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <hr/>

        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('phone', 'Phone:') !!}
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder'=>'Phone']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('skype', 'Skype:') !!}
                {!! Form::text('skype', null, ['class' => 'form-control', 'placeholder'=>'Skype']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::textarea('social_networks', null, ['size' => '30x2', 'class' => 'form-control',
                'placeholder'=>'Social networks links']) !!}
            </div>
        </div>

        <div class="row">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

@stop