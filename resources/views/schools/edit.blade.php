@extends('layouts.app')

@section('content')
    <h1>{{$school->name}}</h1>

    <hr/>

    <div class="row">
        <div class="col-xs-2">
            <div class="form-group">
                <a href="/schools" class="btn btn-success">Back</a>
            </div>
        </div>
        <div class="col-xs-2 col-xs-offset-8">
            {!! Form::open(['method' => 'DELETE', 'route' => ['schools.destroy', $school->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['method' => 'PUT', 'route' => ['schools.update', $school->id]]) !!}
        <div class="row">
            <div class="col-xs-4">
                {!! Form::text('name', $school->name, ['class' => 'form-control', 'placeholder'=>'School name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::select('type_id', $typesArr, $school->type_id, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <h3>{{$school->name}} Campuses</h3>

        {!! Form::open(['method' => 'POST', 'route' => ['schools.campuses.store', $school->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-6">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Campus name']) !!}
            </div>
            <div class="col-xs-6">
                {!! Form::submit('Add Campus', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($school->campuses as $campus)
                <tr>
                    <td>
                        {{$campus->name}}
                    </td>
                    <td>
                        <a href="/schools/{{$school->id}}/campuses/{{$campus->id}}/edit" class="btn btn-success form-control">Edit</a>
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['schools.campuses.destroy', $school->id, $campus->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="well well-lg">
        <h3>{{$school->name}} Contact Persons</h3>

        {!! Form::open(['method' => 'POST', 'route' => ['schools.contact-persons.store', $school->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-6">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Person name']) !!}
            </div>
            <div class="col-xs-6">
                {!! Form::submit('Add Person', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($school->contactPersons as $person)
                <tr>
                    <td>
                        {{$person->name}}
                    </td>
                    <td>
                        <a href="/schools/{{$school->id}}/contact-persons/{{$person->id}}/edit" class="btn btn-success form-control">Edit</a>
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['schools.contact-persons.destroy', $school->id, $person->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="well well-lg">
        <h3>{{$school->name}} Programs</h3>

        {{--{!! Form::open(['method' => 'POST', 'route' => ['schools.programs.store', $school->id]]) !!}--}}
        {{--<div class="form-group">--}}
            {{--@include('programs._form', ['submitButtonText' => 'Add Program', 'programName' => null])--}}
        {{--</div>--}}
        {{--{!! Form::close() !!}--}}

        {{--<table class="table table-bordered table-striped">--}}
            {{--<tbody>--}}
            {{--@foreach($school->programs as $program)--}}
                {{--<tr>--}}
                    {{--<td>--}}
                        {{--{!! Form::open(['method' => 'PUT', 'route' => ['schools.programs.update', $school->id, $program->id]]) !!}--}}
                        {{--@include('programs._form', ['submitButtonText' => 'Save', 'programName' => $program->name])--}}

                        {{--{!! Form::close() !!}--}}
                    {{--</td>--}}
                    {{--<td>--}}
                        {{--{!! Form::open(['method' => 'DELETE', 'route' => ['schools.programs.destroy', $school->id, $program->id]]) !!}--}}
                        {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</td>--}}
                {{--</tr>--}}
            {{--@endforeach--}}
            {{--</tbody>--}}
        {{--</table>--}}
    </div>
@stop