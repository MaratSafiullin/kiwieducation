@extends('layouts.app')

@section('content')
    <div class="row">
        {!! Form::open(['method' => 'GET', 'route' => 'schools.index']) !!}
        <div class="col-xs-3">
            {!! Form::label('name', 'Name(partial):') !!}
            {!! Form::text('name', $schoolName, ['class' => 'form-control', 'placeholder'=>'Name(partial)']) !!}
        </div>
        <div class="col-xs-3">
            {!! Form::label('location', 'Country/City:') !!}
            {!! Form::text('location', $locationName, ['class' => 'form-control', 'placeholder'=>'Location']) !!}
        </div>
        <div class="col-xs-3">
            {!! Form::label('type_id', 'Type:') !!}
            {!! Form::select('type_id', $typesArr, $schoolTypeId, ['class' => 'form-control']) !!}
        </div>
        <div class="col-xs-2">
            {!! Form::label('', '&nbsp;') !!}
            {!! Form::submit('Search', ['class' => 'btn btn-default form-control']) !!}
        </div>
        {!! Form::close() !!}
        <div class="col-xs-1">
            {!! Form::label('', '&nbsp;') !!}
            <a href="/schools" class="btn btn-default  form-control">Reset</a>
        </div>
    </div>

    <h1>Schools</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    @can('create', \App\School::class)
        <div class="well well-lg text-center">
            {!! Form::open(['url' => 'schools']) !!}
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'School name']) !!}
                </div>
                <div class="col-xs-6">
                    {!! Form::submit('Add School', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    @endcan

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($schools as $school)
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-xs-6">
                                {{$school->name}}
                            </div>
                            <div class="col-xs-6">
                                @can('update', $school)
                                    <a href="/schools/{{$school->id}}/edit"
                                       class="btn btn-success form-control">Edit</a>
                                    @else
                                    <a href="/schools/{{$school->id}}"
                                       class="btn btn-default form-control">View</a>
                                @endcan
                            </div>
                        </div>
                    </td>
                    {{--<td>--}}
                    {{--{!! Form::open(['method' => 'DELETE', 'route' => ['schools.destroy', $school->id]]) !!}--}}
                    {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}--}}
                    {{--{!! Form::close() !!}--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop