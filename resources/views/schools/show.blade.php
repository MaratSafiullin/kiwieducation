@extends('layouts.app')

@section('content')
    <h1>{{$school->name}}</h1>

    <hr/>

    <div class="form-group">
        <a href="/schools" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($school, ['method' => 'PUT', 'route' => ['schools.update', $school->id]]) !!}
        <div class="row">
            <div class="col-xs-4">
                {!! Form::text('name', null, ['disabled', 'class' => 'form-control', 'placeholder'=>'School name']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::select('type_id', $typesArr, null, ['disabled', 'class' => 'form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <h3>{{$school->name}} Campuses</h3>

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($school->campuses as $campus)
                <tr>
                    <td>
                        {{$campus->name}}
                    </td>
                    <td>
                        <a href="/schools/{{$school->id}}/campuses/{{$campus->id}}" class="btn btn-default form-control">View</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="well well-lg">
        <h3>{{$school->name}} Contact Persons</h3>

        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($school->contactPersons as $person)
                <tr>
                    <td>
                        {{$person->name}}
                    </td>
                    <td>
                        <a href="/schools/{{$school->id}}/contact-persons/{{$person->id}}" class="btn btn-default form-control">View</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="well well-lg">
        <h3>{{$school->name}} Programs</h3>
    </div>
@stop