@extends('layouts.app')

@section('content')
    <h1>Program Levels</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'program-levels']) !!}
        @include('program-levels._form', ['submitButtonText' => 'Add Level'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($levels as $level)
                <tr>
                    <td>
                        {!! Form::model($level, ['method' => 'PUT', 'route' => ['program-levels.update', $level->id]]) !!}
                        @include('program-levels._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['program-levels.destroy', $level->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop