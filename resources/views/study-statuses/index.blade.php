@extends('layouts.app')

@section('content')
    <h1>Study Statuses</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'study-statuses']) !!}
        @include('study-statuses._form', ['submitButtonText' => 'Add Status'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($studyStatuses as $status)
                <tr>
                    <td>
                        {!! Form::model($status, ['method' => 'PUT', 'route' => ['study-statuses.update', $status->id]]) !!}
                        @include('study-statuses._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['study-statuses.destroy', $status->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop