@extends('layouts.app')

@section('content')
    <h1>School Types</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'school-types']) !!}
        @include('school-types._form', ['submitButtonText' => 'Add Type', 'typeName' => null])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($schoolTypes as $schoolType)
                <tr>
                    <td>
                        {!! Form::open(['method' => 'PUT', 'route' => ['school-types.update', $schoolType->id]]) !!}
                        @include('school-types._form', ['submitButtonText' => 'Save', 'typeName' => $schoolType->name])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['school-types.destroy', $schoolType->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop