@extends('layouts.app')

@section('content')
    <h1>Dashboard</h1>

    <hr/>

    <div class="well well-lg" style="line-height: 150%;">
        <h3>General Settings</h3>

        <div class="row form-group">
            <a href="/commission-statuses" class="btn btn-success">Commission Statuses</a>
            <a href="/duration-units" class="btn btn-success">Duration Units</a>
            <a href="/insurance-types" class="btn btn-success">Insurance Types</a>
        </div>
        <div class="row form-group">
            <a href="/program-levels" class="btn btn-success">Program Levels</a>
            <a href="/student-sources" class="btn btn-success">Student Sources</a>
            <a href="/student-statuses" class="btn btn-success">Student Statuses</a>
            <a href="/study-statuses" class="btn btn-success">Study Statuses</a>
        </div>
        <div class="row form-group">
            <a href="/visa-types" class="btn btn-success">Visa Types</a>
            <a href="/school-types" class="btn btn-success">School Types</a>
        </div>
    </div>

    <div class="well well-lg" style="line-height: 150%;">
        <h3>Company</h3>

        <div class="row form-group">
            <a href="/branches" class="btn btn-success">Branches</a>
            <a href="/users" class="btn btn-success">Users</a>
        </div>
    </div>

    <div class="well well-lg" style="line-height: 150%;">
        <h3>Locations</h3>

        <div class="row form-group">
            <a href="/countries" class="btn btn-success">Countries</a>
            <a href="/cities" class="btn btn-success">Cities</a>
        </div>
    </div>

    <div class="well well-lg" style="line-height: 150%;">
        <h3>Schools</h3>

        <div class="row form-group">
            <a href="/schools" class="btn btn-success">Schools</a>
            <a href="/programs" class="btn btn-success">Programs</a>
            <a href="/program-groups" class="btn btn-success">Program Groups</a>
        </div>
    </div>

    <div class="well well-lg" style="line-height: 150%;">
        <h3>Students</h3>

        <div class="row form-group">
            <a href="/students" class="btn btn-success">Students</a>
            <a href="#" class="btn btn-success">Studies</a>
        </div>
    </div>
@endsection
