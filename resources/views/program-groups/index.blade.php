@extends('layouts.app')

@section('content')
    <div class="row">
        {!! Form::open(['method' => 'GET', 'route' => 'program-groups.index']) !!}
        <div class="col-xs-4">
            {!! Form::label('name', 'Name(partial):') !!}
            {!! Form::text('name', $groupName, ['class' => 'form-control', 'placeholder'=>'Name(partial)']) !!}
        </div>
        <div class="col-xs-4">
            {!! Form::label('school_name', 'School name:') !!}
            {!! Form::text('school_name', $schoolName, ['class' => 'form-control', 'placeholder'=>'School name']) !!}
        </div>
        <div class="col-xs-2">
            {!! Form::label('', '&nbsp;') !!}
            {!! Form::submit('Search', ['class' => 'btn btn-default form-control']) !!}
        </div>
        {!! Form::close() !!}
        <div class="col-xs-2">
            {!! Form::label('', '&nbsp;') !!}
            <a href="/program-groups" class="btn btn-default  form-control">Reset</a>
        </div>
    </div>

    <h1>Program Groups</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    @can('create', \App\ProgramGroup::class)
        <div class="well well-lg text-center">
            {!! Form::open(['url' => 'program-groups']) !!}
            @include('program-groups._form', ['submitButtonText' => 'Add Group'])
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    @endcan

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($programGroups as $programGroup)
                <tr>
                    <td>
                        @can('update', $programGroup)
                            {!! Form::model($programGroup, ['method' => 'PUT', 'route' => ['program-groups.update', $programGroup->id]]) !!}
                            @include('program-groups._form', ['submitButtonText' => 'Save'])
                            {!! Form::close() !!}
                        @else
                            {{$programGroup->name}}
                        @endcan
                    </td>
                    @can('delete', $programGroup)
                        <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['program-groups.destroy', $programGroup->id]]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                            {!! Form::close() !!}
                        </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop