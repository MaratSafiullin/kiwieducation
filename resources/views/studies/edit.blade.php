@extends('layouts.app')

@section('content')
    <h1>
        {{$student->first_name}} {{$student->last_name}} |
        {{$study->school->name}} - {{$study->program->name}}
    </h1>

    <hr/>

    <div class="row">
        <div class="col-xs-2">
            <div class="form-group">
                <a href="/students/{{$student->id}}/edit" class="btn btn-success">Back</a>
            </div>
        </div>
        <div class="col-xs-2 col-xs-offset-8">
            {!! Form::open(['method' => 'DELETE', 'route' => ['students.studies.destroy', $student->id, $study->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($study, ['method' => 'PUT', 'route' => ['students.studies.update', $student->id, $study->id]]) !!}
        <div class="form-group row">
            <div class="col-xs-3">
                {!! Form::label('school_id', 'School:') !!}
                {!! Form::select('school_id', $schoolsArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('program_id', 'Program:') !!}
                {!! Form::select('program_id', $programsArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('branch_id', 'Branch:') !!}
                {!! Form::select('branch_id', $branchesArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('manager_id', 'Manager:') !!}
                {!! Form::select('manager_id', $managersArr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('study_status_id', 'Study status:') !!}
                {!! Form::select('study_status_id', $studyStatusesArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-3">
                {!! Form::label('cost', 'Price:') !!}
                {!! Form::text('cost', null, ['class' => 'form-control', 'placeholder'=>'Price']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('offer_issued_date', 'Offer issued:') !!}
                {!! Form::input('date', 'offer_issued_date', null, ['class' => 'form-control',
                'placeholder'=>'Offer issued date']) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::label('start_date', 'Start date:') !!}
                {!! Form::input('date', 'start_date', null, ['class' => 'form-control',
                'placeholder'=>'Start date']) !!}
            </div>
            <div class="col-xs-1">
                {!! Form::label('duration', 'Duration:') !!}
                {!! Form::text('duration', null, ['class' => 'form-control', 'placeholder'=>'']) !!}
            </div>
            <div class="col-xs-2">
                {!! Form::label('duration_unit_id', '&nbsp;') !!}
                {!! Form::select('duration_unit_id', $durationUnitsArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('agency_commission', 'Agency commisssion:') !!}
                {!! Form::text('agency_commission', null, ['class' => 'form-control', 'placeholder'=>'Agency commisssion']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('agency_commission_collection_date', 'Collection Date:') !!}
                {!! Form::input('date', 'agency_commission_collection_date', null, ['class' => 'form-control',
                'placeholder'=>'Collection Date']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('agency_commission_status_id', 'Status') !!}
                {!! Form::select('agency_commission_status_id', $commissionStatusesArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('chief_commission', 'Chief commisssion:') !!}
                {!! Form::text('chief_commission', null, ['class' => 'form-control', 'placeholder'=>'Chief commisssion']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('chief_commission_collection_date', 'Collection Date:') !!}
                {!! Form::input('date', 'chief_commission_collection_date', null, ['class' => 'form-control',
                'placeholder'=>'Collection Date']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('chief_commission_status_id', 'Status') !!}
                {!! Form::select('chief_commission_status_id', $commissionStatusesArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-4">
                {!! Form::label('manager_commission', 'Manager commisssion:') !!}
                {!! Form::text('manager_commission', null, ['class' => 'form-control', 'placeholder'=>'Manager commisssion']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('manager_commission_collection_date', 'Collection Date:') !!}
                {!! Form::input('date', 'manager_commission_collection_date', null, ['class' => 'form-control',
                'placeholder'=>'Collection Date']) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::label('manager_commission_status_id', 'Status') !!}
                {!! Form::select('manager_commission_status_id', $commissionStatusesArr, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="row">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@stop