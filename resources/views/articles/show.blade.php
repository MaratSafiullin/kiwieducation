@extends('app')

@section('content')
    <h1>{{$article->name}} </h1>

    <article>
        <p>{{$article->text}}</p>
    </article>
@stop