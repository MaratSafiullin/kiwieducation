@extends('app')

@section('content')
    <h1>New Article</h1>
    <hr/>

    {!! Form::open(['url' => 'articles']) !!}
    @include('articles._form', ['submitButtonText' => 'New Article'])
    {!! Form::close() !!}

    @include('errors.list')
@stop