@extends('app')

@section('content')
    <h1>Edit {!! $article->name !!}</h1>
    <hr/>

    {!! Form::model($article, ['method' => 'PUT', 'action' => ['ArticlesController@update', $article->id]]) !!}
    @include('articles._form', ['submitButtonText' => 'Save Article'])
    {!! Form::close() !!}

    @include('errors.list')
@stop