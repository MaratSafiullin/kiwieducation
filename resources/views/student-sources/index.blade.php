@extends('layouts.app')

@section('content')
    <h1>Student Sources</h1>

    <hr/>

    <div class="form-group">
        <a href="/home" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['url' => 'student-sources']) !!}
        @include('student-sources._form', ['submitButtonText' => 'Add Source'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($sources as $source)
                <tr>
                    <td>
                        {!! Form::model($source, ['method' => 'PUT', 'route' => ['student-sources.update', $source->id]]) !!}
                        @include('student-sources._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['student-sources.destroy', $source->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop