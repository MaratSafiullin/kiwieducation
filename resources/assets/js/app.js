$('#student-source-select').unbind('change')
    .change(function () {
        $('#student-source-textfield').val($('#student-source-select option:selected').text());
    });

$('#student-source-textfield').unbind('change')
    .change(function () {
        $('#student-source-select').val(0);
    });

$('#level-ids').select2();
$('#status-ids').select2();
