<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('link')->nullable();
            $table->integer('level_id')->unsigned()->nullable();
            $table->integer('school_id')->unsigned()->nullable();
            $table->integer('group_id')->unsigned()->nullable();
            $table->double('commission_rate')->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('level_id')->references('id')->on('program_levels');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('group_id')->references('id')->on('program_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
