<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->integer('manager_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->integer('school_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->integer('study_status_id')->unsigned()->nullable();
            $table->integer('duration')->unsigned()->nullable();
            $table->integer('duration_unit_id')->unsigned()->nullable();
            $table->date('start_date')->nullable();
            $table->date('offer_issued_date')->nullable();
            $table->double('cost')->nullable();
            $table->double('agency_commission')->nullable();
            $table->integer('agency_commission_status_id')->unsigned()->nullable();
            $table->date('agency_commission_collection_date')->nullable();
            $table->double('chief_commission')->nullable();
            $table->integer('chief_commission_status_id')->unsigned()->nullable();
            $table->date('chief_commission_collection_date')->nullable();
            $table->double('manager_commission')->nullable();
            $table->integer('manager_commission_status_id')->unsigned()->nullable();
            $table->date('manager_commission_collection_date')->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('manager_id')->references('id')->on('users');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('study_status_id')->references('id')->on('study_statuses');
            $table->foreign('duration_unit_id')->references('id')->on('duration_units');
            $table->foreign('agency_commission_status_id')->references('id')->on('commission_statuses');
            $table->foreign('chief_commission_status_id')->references('id')->on('commission_statuses');
            $table->foreign('manager_commission_status_id')->references('id')->on('commission_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studies');
    }
}
