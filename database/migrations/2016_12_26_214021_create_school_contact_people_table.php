<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolContactPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_contact_people', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->integer('campus_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('role')->nullable();
            $table->string('email')->nullable();
            $table->string('skype')->nullable();
            $table->string('phone')->nullable();
            $table->text('social_networks')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('campus_id')->references('id')->on('school_campuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_contact_people');
    }
}
