<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_studies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('school_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->double('tuition_fee')->unsigned()->nullable();
            $table->double('agency_commission')->nullable();
            $table->boolean('agency_commission_paid');
            $table->date('agency_commission_collection_date')->nullable();
            $table->double('manager_commission')->nullable();
            $table->boolean('manager_commission_paid');
            $table->date('manager_commission_collection_date')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('program_id')->references('id')->on('school_programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_studies');
    }
}
