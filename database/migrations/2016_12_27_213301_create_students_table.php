<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('email')->nullable();
            $table->string('skype')->nullable();
            $table->string('phone')->nullable();
            $table->text('social_networks')->nullable();
            $table->string('source')->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('visa_type_id')->unsigned()->nullable();
            $table->date('visa_due_date')->nullable();
            $table->text('visa_comment')->nullable();
            $table->integer('insurance_type_id')->unsigned()->nullable();
            $table->date('insurance_due_date')->nullable();
            $table->text('insurance_comment')->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('status_id')->references('id')->on('student_statuses');
            $table->foreign('visa_type_id')->references('id')->on('visa_types');
            $table->foreign('insurance_type_id')->references('id')->on('insurance_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
