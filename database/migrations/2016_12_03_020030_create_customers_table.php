<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->text('social_networks')->nullable();
            $table->string('source')->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('responsible_manager_id')->unsigned()->nullable();
            $table->date('next_contact_date')->nullable();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('customer_statuses');
            $table->foreign('responsible_manager_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
