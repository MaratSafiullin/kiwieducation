<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('user_role_id')->unsigned()->nullable();
            $table->string('last_name')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('skype')->nullable();
            $table->string('phone')->nullable();
            $table->text('social_networks')->nullable();

            $table->foreign('user_role_id')->references('id')->on('user_roles');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('user_role_id');
            $table->dropColumn('last_name');
            $table->dropColumn('country_id');
            $table->dropColumn('city_id');
            $table->dropColumn('skype');
            $table->dropColumn('phone');
            $table->dropColumn('social_networks');
        });
    }
}
